#include <functional>
#include <queue>
#include <algorithm>

#include "Battle.hpp"
#include "BattleEvent.hpp"

namespace lr{

	Battle::Battle(std::vector <Creature *> &combatants){
		this->combatants = combatants;

		this->battleOptions = Dialog("\nWhat will you do?", {
			"Attack",
			"Defend"
		});


		std::map <std::string, int> names;

		for(auto com : this->combatants){
			if(com->getId() == "player"){
				continue;
			}

			if(names.count(com->getName()) == 0){
				names[com->getName()] = 0;
			} else if(names[com->getName()] == 0){
				names[com->getName()] = 1;
			}
		}

		for(auto &com : this->combatants){
			std::string newName = com->getName();

			if(names.count(com->getName()) > 0 && names[com->getName()] > 0){
				newName += " (" + std::to_string(names[com->getName()]) + ")";
				names[com->getName()] += 1;
			}

			com->setName(newName);
		}
	}

	void Battle::run(){
		std::vector <Creature *>::iterator player;
		std::vector <Creature *>::iterator end;

		do {
			player = std::find_if(this->combatants.begin(), this->combatants.end(),
				[](Creature* a){
					return a->getId() == "player";
			});

			end = this->combatants.end();
			this->nextTurn();
		} while(player != end && this->combatants.size() > 1);

		return;
	}

	void Battle::kill(Creature *creature){
		auto pos = std::find(this->combatants.begin(), this->combatants.end(), creature);

		if(pos != this->combatants.end()){
			rlutil::setColor(12);
			std::cout << "\nVictory!\n\n" << creature->getName() << " is slain!\n\n";
			creature->setHp(0);
			rlutil::resetColor();

			this->combatants.erase(pos);
		}
	}

	void Battle::nextTurn(){
		std::queue <BattleEvent> events;

		std::sort(combatants.begin(), combatants.end(), [](Creature *a, Creature *b) { return a->getAgility() > b->getAgility(); });

		for(auto com : this->combatants){
			if(com->getId() == "player"){
				Dialog targetSelection = Dialog("\nWho?", {});

				for(auto target : this->combatants){
					if(target->getId() != "player"){
						targetSelection.addChoice(target->getName());
					}
				}

				int choice = this->battleOptions.activate();

				if(choice == 1){
					int position = targetSelection.activate();

					for(int i = 0; i < position; i++){
						if(this->combatants[i]->getId() == "player"){
							position++;
						}
					}

					Creature *target = this->combatants[position - 1];
					events.push(BattleEvent(com, target, BattleEventType::ATTACK));
				} else {
					events.push(BattleEvent(com, nullptr, BattleEventType::DEFEND));
				}
			} else {
				Creature *player = *std::find_if(this->combatants.begin(), this->combatants.end(),
					[](Creature *a) { return a->getId() == "player"; });

				events.push(BattleEvent(com, player, BattleEventType::ATTACK));
			}
		}

		while(!events.empty()){
			BattleEvent event = events.front();

			if(event.getType() == BattleEventType::ATTACK){
				auto a = this->combatants.begin();
				auto b = this->combatants.end();

				if(std::find(a, b, event.getSource()) == b || std::find(a, b, event.getTarget()) == b){
					break;
				}

				rlutil::setColor(10);

				std::cout << "\n" << event.getSource()->getName();

				rlutil::resetColor();

				std::cout << " attacks ";

				rlutil::setColor(10);

				std::cout << event.getTarget()->getName();

				rlutil::resetColor();

				std::cout << " for ";

				rlutil::setColor(11);

				std::cout << event.run();

				rlutil::resetColor();

				std::cout << " damage!\n";

				if(event.getTarget()->getHp() <= 0){
					this->kill(event.getTarget());
				} else {
					std::cout << event.getSource()->getName() << " defends!\n";
				}
			}

			events.pop();
		}
	}

};