#include <iostream>

#include "Door.hpp"
#include "Item.hpp"
#include "EntityManager.hpp"

namespace lr{

	Door::Door(std::string id, std::string description, std::pair <std::string,
			std::string> areas, int locked, Item *key) : Entity(id){

		this->description = description;
		this->areas = areas;
		this->locked = locked;
		this->key = key;
	}

	Door::Door(std::string id, Json::Value &data,
				EntityManager* entityManager) : Entity(id){

		this->load(data, entityManager);
	}

	void Door::load(Json::Value &data, EntityManager *entityManager){
		this->description = data["description"].asString();
		this->locked = data["locked"].asInt();

		if(!data["key"].isNull()){
			this->key = entityManager->getEntity <Item> (data["key"].asString());
		}

		if(data["areas"].size() == 2){ // TODO
			this->areas.first = data["areas"][0].asString();
			this->areas.second = data["areas"][1].asString();
		}
	}

};