#include "Item.hpp"
#include "EntityManager.hpp"

namespace lr{

	Item::Item(std::string id, std::string name,
			std::string description) : Entity(id){

		this->name = name;
		this->description = description;
	}

	Item::Item(std::string id, Json::Value &data,
			EntityManager *entityManager) : Entity(id){

		this->load(data, entityManager);
	}

	void Item::load(Json::Value &data, EntityManager *entityManager){
		this->name = data["name"].asString();
		this->description = data["description"].asString();
	}

};