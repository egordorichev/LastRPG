#include <fstream>
#include <string>
#include <cstdlib>
#include <cstring>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <algorithm>

#include "EntityManager.hpp"
#include "Door.hpp"
#include "Item.hpp"
#include "Weapon.hpp"
#include "Armor.hpp"
#include "Creature.hpp"
#include "Area.hpp"

namespace lr{

	EntityManager::EntityManager(){

	}

	EntityManager::~EntityManager(){
		for(auto entity : this->data){
			delete entity.second;
		}
	}

	template <typename T> void EntityManager::loadJson(std::string fileName){
		std::string jsonCode;
        std::stringstream stream;

		std::fstream file(fileName, std::ios::in | std::ios::binary);

		Json::Value root;
		Json::Reader reader;

        stream << file.rdbuf();
		file.close();

    	jsonCode = stream.str();
        jsonCode.erase(std::remove(jsonCode.begin(), jsonCode.end(), '\0'), jsonCode.end());

		if(!reader.parse(jsonCode.c_str(), root)){
			std::cout << "Failed to parse " << fileName << "\n";

			exit(-1);
		}

		auto members = root.getMemberNames();

		for(auto member : members){
			std::string id = member;
			this->data[id] = dynamic_cast <Entity *> (new T(id, root[id], this));
		}
	}

	template <typename T> T *EntityManager::getEntity(std::string id){
		if(id.substr(0, entityToString <T> ().size()) == entityToString <T> ()){
			return dynamic_cast <T *> (this->data[id]);
		} else {
			std::cout << "Wrong type" << std::endl;

			return nullptr;
		}
	}

	template <> std::string entityToString <Item> () { return "item"; }
	template <> std::string entityToString <Weapon> () { return "weapon"; }
	template <> std::string entityToString <Armor> () { return "armor"; }
	template <> std::string entityToString <Creature> () { return "creature"; }
	template <> std::string entityToString <Area> () { return "area"; }
	template <> std::string entityToString <Door> () { return "door"; }

	// Template instantiations
	//
	template void EntityManager::loadJson <Item> (std::string);
	template void EntityManager::loadJson <Weapon> (std::string);
	template void EntityManager::loadJson <Armor> (std::string);
	template void EntityManager::loadJson <Creature> (std::string);
	template void EntityManager::loadJson <Area> (std::string);
	template void EntityManager::loadJson <Door> (std::string);

	template Item* EntityManager::getEntity <Item> (std::string);
	template Weapon* EntityManager::getEntity <Weapon> (std::string);
	template Armor* EntityManager::getEntity <Armor> (std::string);
	template Creature* EntityManager::getEntity <Creature> (std::string);
	template Area* EntityManager::getEntity <Area> (std::string);
	template Door* EntityManager::getEntity <Door> (std::string);

};