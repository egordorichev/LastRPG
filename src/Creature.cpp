#include "Creature.hpp"
#include "Weapon.hpp"
#include "Armor.hpp"
#include "Door.hpp"
#include "EntityManager.hpp"
#include "Area.hpp"

namespace lr{

	Creature::Creature(std::string id, std::string name, int hp,
			int strength, int agility, double evasion, unsigned int experience) : Entity(id){

		this->name = name;
		this->hp = hp;
		this->maxHp = hp;
		this->strength = strength;
		this->agility = agility;
		this->evasion = evasion;
		this->equippedArmor = nullptr;
		this->equippedWeapon = nullptr;
		this->experience = experience;
	}

	Creature::Creature(std::string id, Json::Value &data,
			EntityManager *entityManager) : Creature::Creature(id, "", 0, 0, 0, 0, 0){

		this->load(data, entityManager);
	}

	Creature::Creature(Creature *copy) : Entity(copy->getId()){
		this->name = copy->getName();
		this->hp = copy->getMaxHp();
		this->maxHp = copy->getMaxHp();
		this->strength = copy->getStrength();
		this->agility = copy->getAgility();
		this->evasion = copy->getEvasion();
		this->equippedArmor = nullptr;
		this->equippedWeapon = nullptr;
		this->experience = copy->getExperience();
	}

	void Creature::equipWeapon(Weapon *weapon){
		this->equippedWeapon = weapon;
	}

	void Creature::equipArmor(Armor *armor){
		this->equippedArmor = armor;
	}

	Area *Creature::getAreaPtr(EntityManager *entityManager){
		return entityManager->getEntity <Area> (this->currentArea);
	}

	int Creature::traverse(Door *door){
		int flag = 2;

		if(door->getState() == 0){
			door->setState(-1);
			flag = 2;
		} else if(door->getState() > 0) {
			if(this->inventory.getCount(door->getKey())){
				door->setState(-1);
				flag = 1;
			} else {
				return 0;
			}
		}

		if(door->getAreas().first == this->currentArea){
			this->currentArea = door->getAreas().second;
		} else if(door->getAreas().second == this->currentArea){
			this->currentArea = door->getAreas().first;
		}

		return flag;
	}

	int Creature::attack(Creature* target){
		int damage = 0;

		if(double(std::rand()) / RAND_MAX  > target->getEvasion()){
			int attack = this->strength + (this->equippedWeapon == nullptr ? 0 : this->equippedWeapon->getDamage());
			int defense = target->getAgility() + (target->getEquippedArmor() == nullptr ? 0 : target->getEquippedArmor()->getDefense());

			if(std::rand() % 32 == 0){
				damage = attack / 2 + std::rand() % (attack / 2 + 1);
			} else {
				int baseDamage = attack - defense / 2;
				damage = baseDamage / 4 + std::rand() % (baseDamage / 4 + 1);

				if(damage < 1){
					damage = std::rand() % 2;
				}
			}

			target->reciveDamage(damage);
		}

		return damage;
	}

	Json::Value Creature::getData(){
		Json::Value data;

		data["name"] = this->name;
		data["hp"] = this->hp;
		data["hp_max"] = this->maxHp;
		data["strength"] = this->strength;
		data["agility"] = this->agility;
		data["evasion"] = this->evasion;
		data["experience"] = this->experience;
		data["inventory"] = Json::Value(this->inventory.getData());
		data["equipped_weapon"] = Json::Value(this->equippedWeapon == nullptr ? "nullptr" : this->equippedWeapon->getId());
		data["equipped_armor"] = Json::Value(this->equippedArmor == nullptr ? "nullptr" : this->equippedArmor->getId());

		return data;
	}

	void Creature::load(Json::Value &data, EntityManager *entityManager){
		this->name = data["name"].asString();
		this->hp = data["hp"].asInt();

		if(!data["hp_max"].isNull()){
			this->maxHp = data["hp_max"].asInt();
		} else {
			this->maxHp = hp;
		}

		this->strength = data["strength"].asInt();
		this->agility = data["agility"].asInt();
		this->evasion = data["evasion"].asFloat();
		this->experience = data["experience"].asInt();

		if(!data["inventory"].isNull()){
			this->inventory = Inventory(data["inventory"], entityManager);
		}

		if(!data["equipped_weapon"].isNull()){
			std::string equippedWeaponName = data["equipped_weapon"].asString();

			this->equippedWeapon = equippedWeaponName == "nullptr" ? nullptr :
				entityManager->getEntity <Weapon> (equippedWeaponName);
		}

		if(!data["equipped_armor"].isNull()){
			std::string equippedArmorName = data["equipped_armor"].asString();

			this->equippedArmor = equippedArmorName == "nullptr" ? nullptr :
				entityManager->getEntity <Armor> (equippedArmorName);
		}
	}

};