#ifndef COMMAND_HPP_
#define COMMAND_HPP_

#include <functional>
#include <string>

namespace lr{

	class Command{
		public:
			Command(std::string name, std::string description, std::function <void ()> command) : name(name),
				description(description), command(command){ };

			Command();

			void run(){ this->command(); };

			std::string getName(){ return this->name; };
			std::string getDescription(){ return this->description; };
		private:
			std::string name;
			std::string description;
			std::function <void ()> command;
	};

};

#endif // COMMAND_HPP_