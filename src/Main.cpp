#include <iostream>
#include <ctime>
#include <fstream>

#include "rlutil.h"

#include "Player.hpp"
#include "Area.hpp"
#include "Dialog.hpp"
#include "Door.hpp"
#include "Command.hpp"
#include "Armor.hpp"
#include "Weapon.hpp"
#include "Battle.hpp"

#define VERSION 0.1
#define VERSION_STRING "0.1"

using namespace lr;

EntityManager entityManager;
Player player;
Area *areaPtr;

std::vector <Command> commands;

void printLogo(){
	rlutil::setColor(8); // Grey

	std::cout <<
		" _               _  ____________ _____ \n" <<
	   "| |             | | | ___ \\ ___ \\  __ \\\n" <<
	   "| |     __ _ ___| |_| |_/ / |_/ / |  \\/\n" <<
	   "| |    / _` / __| __|    /|  __/| | __ \n" <<
	   "| |___| (_| \\__ \\ |_| |\\ \\| |   | |_\\ \\\n" <<
	   "\\_____/\\__,_|___/\\__\\_| \\_\\_|    \\____/\n" << "\n\n";

	rlutil::resetColor();
}

void printHelp(){
	std::cout << "Last RPG\n" <<
		"\thelp, -h\t\tPrint help\n" <<
		"\tTODO\n";

	exit(0);
}

Player startGame(){
	entityManager.loadJson <Item> ("data/items.json");
	entityManager.loadJson <Weapon> ("data/weapons.json");
	entityManager.loadJson <Armor> ("data/armor.json");
	entityManager.loadJson <Creature> ("data/creatures.json");
	entityManager.loadJson <Door> ("data/doors.json");
	entityManager.loadJson <Area> ("data/areas.json");

	std::srand(std::time(nullptr));

	std::string name = ask("What is your name?");
	std::ifstream file("data/" + name + ".json");

	if(file.good()){
		Json::Value saveData;
		Json::Value areaData;

		file >> saveData;

		file.close();
		file.open("data/" + name + "_areas.json");

		file >> areaData;

		file.close();

		Player player = Player(saveData, areaData, &entityManager);

		return player;
	} else {
		file.close();

		int result = Dialog(name + ", please, choose your class: ", { "Fighter", "Rogue" }).activate();

		switch(result){
			// Fighter class favours strength
			case 1: return Player(name, 15, 5, 4, 1.0/64.0, 0, 1, "Fighter");
			// Rogue class favours agility
			case 2: return Player(name, 15, 4, 5, 1.0/64.0, 0, 1, "Rogue");
			// Default case that should never happen, but it's good to be safe
			default: return Player(name, 15, 4, 4, 1.0/64.0, 0, 1, "Adventurer");
		}
	}
}

void runGame(){
	player.setCurrentArea("area_01");
	rlutil::cls();
	std::cout << player.getAreaPtr(&entityManager)->getDialog()->getDescription() << "\n";

	while(1){
		areaPtr = player.getAreaPtr(&entityManager);
		player.save(&entityManager);

		if(areaPtr->getCreatures().size() > 0){
			std::vector <Creature *> combatants;
			std::cout << "You are attacked by ";

			for(int i = 0; i < areaPtr->getCreatures().size(); i++){
				Creature *c = &(areaPtr->getCreatures()[i]);
				combatants.push_back(c);

				std::cout << c->getName() << (i == areaPtr->getCreatures().size()-1 ? "!\n" : ", ");
			}

			combatants.push_back(&player);

			Battle battle(combatants);
			battle.run();

			if(player.getHp() > 0){
				unsigned int xp = 0;

				for(auto creature : areaPtr->getCreatures()){
					xp += creature.getExperience();
				}

				std::cout << "You gained " << xp << " experience!\n";

				player.reciveExperince(xp);
				areaPtr->getCreatures().clear();

				continue;
			} if(player.getHp() <= 0){
				std::cout << "You died. Game over!\n";

				return;
			}
		}

		std::string action = ask("You:");
		bool found = false;

		for(auto command : commands){
			if(command.getName() == action ||
					std::string(1, command.getName()[0]) == action){

				command.run();
				found = true;

				break;
			}
		}

		if(!found){
			rlutil::setColor(4);
			std::cout << "Sorry, command is invalid. Try to type \"help\" for more info\n";
			rlutil::resetColor();
		}
	}
}

void settings(){

}

int main(int argc, char const *argv[]){
	rlutil::cls();
	std::srand(std::time(nullptr));
	printLogo();

	commands.push_back(Command("help", "Show this hint", [&](){
		std::cout << "Commands: \n";

		for(auto command : commands){
			rlutil::setColor(11);

			std::cout << "\t[" << command.getName()[0] << "]" << command.getName().erase(0, 1);

			rlutil::resetColor();

			std::cout << "\t\t" << command.getDescription() << "\n";
		}
	}));

	commands.push_back(Command("look", "Look around", [&](){
		std::cout << player.getAreaPtr(&entityManager)->getDialog()->getDescription() << "\n";

		auto areaItems = areaPtr->getItems();
		auto playerItems = player.getItems();

		if(!areaItems->isEmpty()){
			rlutil::setColor(6);
			std::cout << "\nYou found:\n" << std::endl;
			rlutil::resetColor();

			areaItems->print();
			playerItems->merge(areaItems);
			areaItems->clear();

			std::cout << "\n";
		}
	}));

	commands.push_back(Command("items", "Shows your inventory", [&](){
		int userInput = 0;
		int numItems = 0;

		switch(Dialog("Inventory Menu ======", { "Your Items",
				"Equipment", "Character", "Close" }).activate()){

			case 1:
				std::cout << "Your Items =====\n\n";
				player.getItems()->print();
				std::cout << "\n";
			break;
			case 2:
				std::cout << "Equipment =====\n\n";
				std::cout << "Armor: " << (player.getEquippedArmor() != nullptr ?
					player.getEquippedArmor()->getName() : "Nothing") << "\n";
				std::cout << "Weapon: " << (player.getEquippedWeapon() != nullptr ?
					player.getEquippedWeapon()->getName() : "Nothing") << "\n\n";

				switch(Dialog("", { "Equip Armor", "Equip Weapon", "Close" }).activate()){
					case 1:
						std::cout << "\n";

						userInput = 0;
						numItems = player.getItems()->print <Armor> (true);

						std::cout << "\n";

						if(numItems == 0){
							break;
						}

						while(!userInput){
							userInput = std::stoi(ask(""));

							if(userInput >= 1 && userInput <= numItems){
								player.equipArmor(player.getItems()->get <Armor> (userInput - 1));
							}
						}
					break;
					case 2:
						std::cout << "\n";

						userInput = 0;
						numItems = player.getItems()->print <Weapon> (true);

						std::cout << "\n";

						if(numItems == 0){
							break;
						}

						while(!userInput){
							userInput = std::stoi(ask(""));

							if(userInput >= 1 && userInput <= numItems){
								player.equipWeapon(player.getItems()->get <Weapon> (userInput - 1));
							}
						}
					break;
					case 3: break;
				}
			break;
			case 3:
				std::cout << "Character ======\n\n";
				std::cout << player.getName();

				if(player.getClassName() != ""){
					std::cout << " the " << player.getClassName();
				}

				std::cout << "\n\n";

				std::cout << "Health:   \t" << player.getHp() << " / " << player.getMaxHp() << "\n";
				std::cout << "Strength: \t" << player.getStrength() << "\n";
				std::cout << "Agility:  \t" << player.getAgility() << "\n";
				std::cout << "Level:    \t" << player.getLevel() << " (" << player.getExperience();
				std::cout <<  " / " << player.experienceToLevel(player.getLevel() + 1) << ")" << "\n\n";
			break;
			case 4: break;
		}
	}));

	commands.push_back(Command("move", "Move to neighboring rooms", [&](){
		Dialog variants("Where would you like to move?", {});

		for(auto door : areaPtr->getDoors()){
			variants.addChoice("Go through the " + door->getDescription());
		}

		int result = variants.activate();

		Door *door = areaPtr->getDoors().at(result - areaPtr->getDialog()->getSize() - 1);

		int flag = player.traverse(door);

		switch(flag){
			default:
			case 0:
				std::cout << "The " << door->getDescription() << " is locked.\n";
			break;
			case 1:
				std::cout << "You unlock the " << door->getDescription() << " and go through it.\n";
			break;
			case 2:
				std::cout << "You go through the " << door->getDescription() << ".\n";
			break;
		}
	}));


	if(argc > 1){ // Parse args
		for(int i = 1; i < argc; i++){
			if(std::string(argv[i]) == "help" ||
					std::string(argv[i]) == "-h"){

				printHelp();
			} else {
				printHelp();
			}
		}
	}

	switch(Dialog("", { "Start game", "Settings",
			"Exit" }).activate()){

		case 1: // Start game
			player = startGame();

			runGame();
		break;
		case 2: // Settings
			settings();
		break;
		case 3: // Exit
			exit(0);
		break;
	}

	return 0;
}