#include "Area.hpp"
#include "Item.hpp"
#include "Door.hpp"
#include "Creature.hpp"

namespace lr{

	Area::Area(std::string id, Dialog &dialog, Inventory &items, std::vector <Creature *> creatures) : Entity(id){
		this->dialog = dialog;
		this->items = items;

		for(auto creature : creatures){
			this->creatures.push_back(*creature);
		}
	}

	Area::Area(std::string id, Json::Value &data, EntityManager *entityManager) : Entity(id){
		this->load(data, entityManager);
	}

	void Area::load(Json::Value &data, EntityManager *entityManager){
		if(!data["dialog"].isNull()){
			this->dialog = Dialog(data["dialog"]);
		}

		this->items = Inventory(data["inventory"], entityManager);

		this->creatures.clear();

		for(auto creature : data["creatures"]){
			Creature c(entityManager->getEntity <Creature> (creature.asString()));
			this->creatures.push_back(c);
		}

		if(!data["doors"].isNull()){
			this->doors.clear();

			for(auto door : data["doors"]){
				Door *d = nullptr;

				if(door.isString()){
					d = entityManager->getEntity <Door> (door.asString());
				} else {
					d = entityManager->getEntity <Door> (door[0].asString());
					d->setState(door[1].asInt());
				}

				this->doors.push_back(d);
			}
		}
	}

	Json::Value Area::getData(){
		Json::Value data;

		data["inventory"] = this->items.getData();

		Json::Value doorsData;

		for(auto door : this->doors){
			Json::Value doorData;

			doorData.append(door->getId());
			doorData.append(door->getState());

			doorsData.append(doorData);
		}

		data["doors"] = doorsData;

		return data;
	}

};