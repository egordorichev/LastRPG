#include <fstream>
#include <cmath>
#include <iostream>

#include "Player.hpp"
#include "Area.hpp"
#include "Armor.hpp"
#include "Weapon.hpp"

namespace lr{

	Player::Player(std::string name, int hp, int strength,
			int agility, double evasion, unsigned int experience,
			unsigned int level, std::string className) : Creature("player",
			name, hp, strength, agility, evasion, experience){

		this->level = level;
		this->className = className;
	}

	Player::Player(Json::Value &data, Json::Value &areaData,
		 	EntityManager *entityManager) : Player::Player(){

		this->load(data, entityManager);
		this->loadArea(areaData, entityManager);
	}

	Player::Player() : Player::Player("", 0, 0, 0, 0.0, 0, 1, "nullid"){

	}

	unsigned int Player::experienceToLevel(unsigned int level){
		return (unsigned int)(1.5 * std::pow(this->level, 3));
	}

	bool Player::levelUp(){
		if(this->experience < experienceToLevel(this->level + 1)){
			return false;
		}

		level++;

		unsigned int statIncreases[3] = {0, 0, 0};
		float statMultipliers[3] = {0, 0, 0};

		statMultipliers[0] = 13.0;
		statMultipliers[1] = this->className == "Fighter" ? 8.0 : 6.0;
		statMultipliers[2] = this->className == "Rogue" ? 8.0 : 6.0;

		for(int i = 0; i < 3; ++i){
			float base = std::tanh(this->level / 30.0) * ((this->level % 2) + 1);
			statIncreases[i] += int(1 + statMultipliers[i] * base);
		}

		this->hp += statIncreases[0];
		this->maxHp += statIncreases[0];
		this->strength += statIncreases[1];
		this->agility += statIncreases[2];

		std::cout << this->name << " grew to level " << level << "!\n"; // TODO
		std::cout << "Health   +" << statIncreases[0] << " -> " << this->maxHp << std::endl;
		std::cout << "Strength +" << statIncreases[1] << " -> " << this->strength << std::endl;
		std::cout << "Agility  +" << statIncreases[2] << " -> " << this->agility << std::endl;
		std::cout << "----------------\n";

		return true;
	}

	Json::Value Player::getData(){
		Json::Value data = Creature::getData();

		data["class_name"] = this->name;
		data["level"] = this->level;

		return data;
	}

	void Player::save(EntityManager *entityManager){
		std::ofstream file("data/" + this->name + ".json", std::ios::trunc);

		file << this->getData();
		file.close();
		file.open("data/" + this->name + "_areas.json", std::ios::trunc);

		Json::Value data;

		for(auto area : this->visitedAreas){
			data[area] = entityManager->getEntity <Area> (area);
		}

		file << data;
		file.close();
	}

	void Player::load(Json::Value &data, EntityManager *entityManager){
		Creature::load(data, entityManager);

		this->className = data["class_name"].asString();
		this->level = data["level"].asInt();
	}

	void Player::loadArea(Json::Value &data, EntityManager *entityManager){
		for(auto area : data){
				std::string key = area[0].asString();

				entityManager->getEntity <Area> (key)->load(area[1], entityManager);

				this->visitedAreas.insert(key);
		}
	}

};