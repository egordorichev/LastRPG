#include "Armor.hpp"
#include "EntityManager.hpp"

namespace lr{

	Armor::Armor(std::string id, std::string name,
			std::string description, int defense) : Item(id, name, description){

		this->defense = defense;
	}

	Armor::Armor(std::string id, Json::Value &data,
			EntityManager *entityManager) : Item(id, data, entityManager){

		this->load(data, entityManager);
	}

	void Armor::load(Json::Value &data, EntityManager *entityManager){
		this->defense = data["defense"].asInt();
	}

};