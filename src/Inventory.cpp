#include <iostream>

#include "rlutil.h"

#include "Inventory.hpp"
#include "Weapon.hpp"
#include "Armor.hpp"

namespace lr{

	Inventory::Inventory(Json::Value &data, EntityManager *entityManager){
		this->load <Item> (data["items"], entityManager);
		this->load <Weapon> (data["weapons"], entityManager);
		this->load <Armor> (data["armor"], entityManager);
	}

	Inventory::Inventory(){

	}

	template <typename T> void Inventory::load(Json::Value &data, EntityManager *entityManager){
		if(!data.isNull()){
			for(auto item : data){
				std::string id = item[0].asString();
				int quantity = item[1].asInt();

				this->items.push_back(std::make_pair(entityManager->getEntity <T> (id),
					quantity));
			}
		}
	}

	template <typename T> Json::Value Inventory::asArray(){
		Json::Value data;

		for(auto item : this->items){
			if(item.first->getId().substr(0, entityToString <T> ().size()) !=
					entityToString <T> ()){

            	continue; // Skip
			}

			Json::Value pair;

			pair.append(item.first->getId());
			pair.append(item.second);

			data.append(pair);
		}

		return data;
	}

	Json::Value Inventory::getData(){
		Json::Value data;

		data["items"] = Json::Value(this->asArray <Item> ());
		data["weapons"] = Json::Value(this->asArray <Weapon> ());
		data["armor"] = Json::Value(this->asArray <Armor> ());

		return data;
	}

	void Inventory::add(Item *item, int count){
		for(auto it : this->items){
			if(it.first->getId() == item->getId()){
				it.second += count;

				return;
			}
		}

		this->items.push_back(std::make_pair(item, count));
	}

	void Inventory::remove(Item *item, int count){
		for(auto it = this->items.begin(); it != this->items.end(); ++it){
			if((*it).first->getId() == item->getId()){
				(*it).second -= count;

				if((*it).second < 1){
					this->items.erase(it);
				}

				return;
			}
		}
	}

	void Inventory::clear(){
		this->items.clear();
	}

	void Inventory::merge(Inventory *inventory){
		if(inventory == this){
			return;
		}

    	for(auto it : inventory->items){
			this->add(it.first, it.second);
		}

    	return;
	}

	int Inventory::getCount(Item *item){
		for(auto it : this->items){
	        if(it.first->getId() == item->getId()){
	            return it.second;
			}
    	}

		return 0;
	}

	int Inventory::print(bool label){
		unsigned int i = 0;

	    if(items.empty()){
	        std::cout << "Nothing\n";
	    } else {
	        i += print <Item> (label);
	        i += print <Weapon> (label);
	        i += print <Armor> (label);
	    }

	    return i;
	}

	template <typename T> int Inventory::getCount(unsigned int number){
		return getCount(get <T> (number));
	}

	template <typename T> T *Inventory::get(unsigned int number){
		unsigned int i = 0;
	    auto it = this->items.begin();

	    for(; it != this->items.end(); ++it){
	        if((*it).first->getId().substr(0,
					entityToString <T> ().size()) != entityToString <T> ()){

	            continue;
			}

	        if(i++ == number){
				break;
			}
	    }

	    if(it != this->items.end()){
	        return dynamic_cast <T*> ((*it).first);
	    } else {
	        return nullptr;
		}
	}

	template <typename T> int Inventory::print(bool label){
		unsigned int i = 1;

		for(auto it : this->items){
	        if(it.first->getId().substr(0, entityToString <T> ().size()) != entityToString <T> ()){
	            continue;
			}

	        if(label){
				rlutil::setColor(9);

				std::cout << "[" << i++ << "] ";

				rlutil::resetColor();
			}

	        std::cout << it.first->getName();
			rlutil::setColor(11);
			std::cout << " (" << it.second << ")";
			rlutil::resetColor();

			if(it.first->getDescription() != ""){
				std::cout << " - " << it.first->getDescription() << "\n";
			}
		}

    	return this->items.size();
	}

	template void Inventory::load <Item> (Json::Value &, EntityManager *);
	template void Inventory::load <Weapon> (Json::Value &, EntityManager *);
	template void Inventory::load <Armor> (Json::Value &, EntityManager *);

	template Json::Value Inventory::asArray <Item> ();
	template Json::Value Inventory::asArray <Weapon> ();
	template Json::Value Inventory::asArray <Armor> ();

	template int Inventory::getCount <Item> (unsigned int);
	template int Inventory::getCount <Weapon> (unsigned int);
	template int Inventory::getCount <Armor> (unsigned int);

	template Item *Inventory::get <Item> (unsigned int);
	template Weapon *Inventory::get <Weapon> (unsigned int);
	template Armor *Inventory::get <Armor> (unsigned int);

	template int Inventory::print <Item> (bool);
	template int Inventory::print <Weapon> (bool);
	template int Inventory::print <Armor> (bool);

};