#include "Weapon.hpp"
#include "EntityManager.hpp"

namespace lr{

	Weapon::Weapon(std::string id, std::string name,
			std::string description, int damage) : Item(id, name, description){

		this->damage = damage;
	}

	Weapon::Weapon(std::string id, Json::Value &data,
			EntityManager *entityManager) : Item(id, data, entityManager){

		this->load(data, entityManager);
	}

	void Weapon::load(Json::Value &data, EntityManager *entityManager){
		this->damage = data["damage"].asInt();
	}

};