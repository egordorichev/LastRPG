#include "Dialog.hpp"

namespace lr{

	std::string ask(std::string question){
		rlutil::setColor(6);

		std::cout << "[?] ";

		rlutil::setColor(15);

		if(question != ""){
			std::cout << question << " ";
		}
		
		rlutil::setColor(11);

		std::string answer;
		std::cin >> answer;

		rlutil::resetColor();

		return answer;
	}

	Dialog::Dialog(std::string description, std::vector <std::string> choices){
		this->description = description;
		this->choices = choices;
	}

	Dialog::Dialog(Json::Value &value){
		if(!value.isNull()){
			this->description = value["description"].asString();

			for(auto choice : value["choices"]){
				this->choices.push_back(choice.asString());
			}
		}
	}

	Dialog::Dialog(){
		// Do nothing
	}

	int Dialog::activate(){
		rlutil::setColor(15);
		if(this->description != ""){
			std::cout << this->description << "\n\n";
		}

		for(int i = 0; i < this->choices.size(); i++){
			rlutil::setColor(9);

			std::cout << "[" << i + 1 << "] ";

			rlutil::resetColor();

			std::cout << this->choices[i] << "\n";
		}

		rlutil::resetColor();

		int userInput = -1;

		std::cout << "\n";

		while(1){
			rlutil::setColor(6);

			std::cout << "[?] ";

			rlutil::setColor(15);

			std::cout << "You: ";

			rlutil::setColor(11);

			std::cin >> userInput;

			if(userInput >= 0 && userInput <= this->choices.size()){
				rlutil::resetColor();

				return userInput;
			} else {
				rlutil::setColor(4);

				std::cerr << "Input is out of range\n";
			}
		}

		rlutil::resetColor();
	}

};