#ifndef PLAYER_HPP_
#define PLAYER_HPP_

#include <string>
#include <unordered_set>

#include "jsoncpp/json.h"

#include "Creature.hpp"

namespace lr{

	class Player : public Creature{
		public:
			Player(std::string name, int hp, int strength, int agility, double evasion,
				unsigned int experience, unsigned int level, std::string className);
			Player(Json::Value &saveData, Json::Value &areaData, EntityManager *entityManager);
			Player();

			unsigned int experienceToLevel(unsigned int level);
			int getLevel(){ return this->level; };

			bool levelUp();

			Json::Value getData();

			void save(EntityManager *entityManager);
			void load(Json::Value &data, EntityManager *entityManager);
			void loadArea(Json::Value &data, EntityManager *entityManager);
			void setCurrentArea(std::string area){ this->currentArea = area; this->visitedAreas.insert(area); };

			std::string getClassName(){ return this->className; };
		private:
			std::string className;
			std::unordered_set <std::string> visitedAreas;

			int level;
	};

};

#endif // PLAYER_HPP_