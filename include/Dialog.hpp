#ifndef DIALOG_HPP_
#define DIALOG_HPP_

#include <string>
#include <vector>
#include <iostream>

#include "rlutil.h"

#include "jsoncpp/json.h"

namespace lr{

	std::string ask(std::string question);

	class Dialog{
		public:
			Dialog(std::string description, std::vector <std::string> choices);
			Dialog(Json::Value &value);
			Dialog();

			int activate();

			void addChoice(std::string choise){ this->choices.push_back(choise); };
			unsigned int getSize(){ return this->choices.size(); };

			std::string getDescription(){ return this->description; };
		private:
			std::string description;
			std::vector <std::string> choices;
	};

};

#endif // DIALOG_HPP_