#ifndef ENTITY_HPP_
#define ENTITY_HPP_

#include <string>

#include "jsoncpp/json.h"

namespace lr{

	class EntityManager;

	class Entity{
		public:
			Entity(std::string id){ this->id = id; };

			virtual ~Entity(){ };
			virtual void load(Json::Value &data, EntityManager *entityManager){ };

			std::string getId(){ return this->id; };
		private:
			std::string id;
	};

};

#endif // ENTITY_HPP_