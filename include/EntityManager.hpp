#ifndef ENTITYMANAGER_HPP_
#define ENTITYMANAGER_HPP_

#include <string>
#include <map>

#include "jsoncpp/json.h"

#include "Entity.hpp"

namespace lr{

	template <typename T> std::string entityToString();

	class EntityManager{
		public:
			EntityManager();
			~EntityManager();

			template <typename T> void loadJson(std::string fileName);
			template <typename T> T *getEntity(std::string id);
		private:
			std::map <std::string, Entity *> data;
	};

};

#endif // ENTITYMANAGER_HPP_