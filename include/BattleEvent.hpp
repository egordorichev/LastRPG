#ifndef BATTLEEVENT_HPP_
#define BATTLEEVENT_HPP_

namespace lr{

	class Creature;

	enum BattleEventType{
		ATTACK,
		DEFEND
	};

	class BattleEvent{
		public:
			BattleEvent(Creature *source, Creature *target, BattleEventType type) : source(source),
				target(target), type(type){ };

			int run(){
				switch(type){
					case BattleEventType::ATTACK: return source->attack(target);
					case BattleEventType::DEFEND: return 0;
					default: return 0;
				}
			}

			BattleEventType getType(){ return this->type; };

			Creature *getSource(){ return this->source; };
			Creature *getTarget(){ return this->target; };
		private:
			Creature *source;
			Creature *target;

			BattleEventType type;
	};

};

#endif // BATTLEEVENT_HPP_