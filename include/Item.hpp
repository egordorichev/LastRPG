#ifndef ITEM_HPP_
#define ITEM_HPP_

#include "Entity.hpp"

namespace lr{

	class EntityManager;

	class Item : public Entity{
		public:
			Item(std::string id, std::string name, std::string description);
			Item(std::string id, Json::Value &data, EntityManager *entityManager);

			virtual void load(Json::Value &data, EntityManager *entityManager);

			std::string getName(){ return this->name; };
			std::string getDescription(){ return this->description; };
		private:
			std::string name;
			std::string description;
	};

};

#endif // ITEM_HPP_