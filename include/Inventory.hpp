#ifndef INVENTORY_HPP_
#define INVENTORY_HPP_

#include <list>
#include <utility>

#include "jsoncpp/json.h"

#include "Item.hpp"
#include "EntityManager.hpp"

namespace lr{

	class Inventory{
		public:
			Inventory(Json::Value &data, EntityManager *entityManager);
			Inventory();

			void add(Item *item, int count);
			void remove(Item *item, int count);
			void clear();
			void merge(Inventory *inventory);

			int getCount(Item *item);
			int print(bool label = false);

			Json::Value getData();

			template <typename T> int getCount(unsigned int number);
			template <typename T> T *get(unsigned int number);
			template <typename T> int print(bool label = false);

			bool isEmpty(){ return this->items.empty(); };
		private:
			std::list <std::pair <Item *, int>> items;

			template <typename T> void load(Json::Value &data, EntityManager *entityManager);
			template <typename T> Json::Value asArray();

	};

};

#endif // INVENTORY_HPP_