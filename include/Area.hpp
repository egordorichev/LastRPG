#ifndef AREA_HPP_
#define AREA_HPP_

#include <string>
#include <utility>
#include <vector>

#include "jsoncpp/json.h"

#include "Entity.hpp"
#include "Dialog.hpp"
#include "Inventory.hpp"

namespace lr{

	class Item;
	class Door;
	class Creature;

	class Area : public Entity{
		public:
			Area(std::string id, Dialog &dialog, Inventory &items, std::vector <Creature *> creatures);
			Area(std::string id, Json::Value &data, EntityManager *entityManager);

			void load(Json::Value &data, EntityManager *entityManager);

			Json::Value getData();

			Dialog *getDialog(){ return &this->dialog; };
			Inventory *getItems(){ return &this->items; };

			std::vector <Door *> &getDoors(){ return this->doors; };
			std::vector <Creature> &getCreatures(){ return this->creatures; };
		private:
			Dialog dialog;
			Inventory items;

			std::vector <Creature> creatures;
			std::vector <Door *> doors;
	};

};

#endif // AREA_HPP_