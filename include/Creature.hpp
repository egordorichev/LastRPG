#ifndef CREATURE_HPP_
#define CREATURE_HPP_

#include <string>
#include <cstdlib>

#include "jsoncpp/json.h"

#include "Entity.hpp"
#include "Inventory.hpp"

namespace lr{

	class Area;
	class EntityManager;
	class Weapon;
	class Armor;
	class Door;

	class Creature : public Entity{
		public:
			Creature(std::string id, std::string name, int hp,
				int strength, int agility, double evasion, unsigned int experience);

			Creature(std::string id, Json::Value &data, EntityManager *entityManager);
			Creature(Creature *copy);

			void equipWeapon(Weapon *weapon);
			void equipArmor(Armor *armor);
			void reciveDamage(int damage){ this->hp -= damage; };
			void setName(std::string name){ this->name = name; };
			void setHp(int hp){ this->hp = hp; };
			void reciveExperince(int experience){ this->experience = experience; };

			Area *getAreaPtr(EntityManager *entityManager);
			Inventory *getItems(){ return &this->inventory; };
			Weapon *getEquippedWeapon(){ return this->equippedWeapon; };
			Armor *getEquippedArmor(){ return this->equippedArmor; };

			int attack(Creature *target);
			int traverse(Door *door);

			virtual Json::Value getData();
			virtual void load(Json::Value &data, EntityManager *entityManager);

			std::string getName(){ return this->name; };

			int getHp(){ return this->hp; };
			int getMaxHp(){ return this->maxHp; };
			int getStrength(){ return this->strength; };
			int getAgility(){ return this->agility; };
			double getEvasion(){ return this->evasion; };
			unsigned int getExperience(){ return this->experience; };
		protected:
			Inventory inventory;
			Weapon* equippedWeapon;
			Armor* equippedArmor;

			std::string currentArea;
			std::string name;

			int hp;
			int maxHp;
			int strength;
			int agility;
			double evasion;
			unsigned int experience;
	};

};

#endif // CREATURE_HPP_