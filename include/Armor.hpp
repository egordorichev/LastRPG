#ifndef ARMOR_HPP_
#define ARMOR_HPP_

#include "Item.hpp"

namespace lr{

	class Armor : public Item{
		public:
			Armor(std::string id, std::string name, std::string description, int defense);
			Armor(std::string id, Json::Value &data, EntityManager *entityManager);

			void load(Json::Value &data, EntityManager *entityManager);

			int getDefense(){ return this->defense; };
		private:
			int defense;
	};

};

#endif // ARMOR_HPP_