#ifndef BATTLE_HPP_
#define BATTLE_HPP_

#include <vector>

#include "Creature.hpp"
#include "Dialog.hpp"

namespace lr{

	class Battle{
		public:
			Battle(std::vector <Creature *> &combatants);

			void run();
		private:
			std::vector <Creature *> combatants;
			Dialog battleOptions;

			void kill(Creature *creature);
			void nextTurn();
	};

};

#endif // BATTLE_HPP_