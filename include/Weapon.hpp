#ifndef WEAPON_HPP_
#define WEAPON_HPP_

#include "Item.hpp"

namespace lr{

	class Weapon : public Item{
		public:
			Weapon(std::string id, std::string name, std::string description, int damage);
			Weapon(std::string id, Json::Value &data, EntityManager *entityManager);

			void load(Json::Value &data, EntityManager *entityManager);

			int getDamage(){ return this->damage; };
		private:
			int damage;
	};

};

#endif // WEAPON_HPP_