#ifndef DOOR_HPP_
#define DOOR_HPP_

#include <string>
#include <utility>

#include "jsoncpp/json.h"

#include "Entity.hpp"

namespace lr{

	class Item;
	class EntityManager;

	class Door : public Entity{
		public:
			Door(std::string id, std::string description, std::pair <std::string,
				std::string> areas, int locked, Item *key = nullptr);

			Door(std::string id, Json::Value &data, EntityManager* entityManager);

			void load(Json::Value &data, EntityManager *entityManager);
			void setState(int state){ this->locked = locked; };
			int getState(){ return this->locked; };

			std::string getDescription(){ return this->description; };
			std::pair <std::string, std::string> &getAreas(){ return this->areas; };

			Item *getKey(){ return this->key; };
		private:
			int locked;

			std::string description;
			std::pair <std::string, std::string> areas;

			Item *key;
	};

};

#endif // DOOR_HPP_